function inKetQua() {
  var maSV = document.getElementById("txtMaSV").value;
  var tenSV = document.getElementById("txtTenSV").value;
  var loaiSV = document.getElementById("loaiSV").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemVan = document.getElementById("txtDiemVan").value * 1;

  var user = {
    ma: maSV,
    ten: tenSV,

    loai: loaiSV,
    diemToan: diemToan,
    diemVan: diemVan,
    tinhDTB: function () {
      return (this.diemToan + this.diemVan) / 2;
    },
    xepLoai: function () {
      if (this.tinhDTB() >= 5) {
        return "Đạt";
      } else {
        return "Không đạt";
      }
    },
  };
  // console.log("user: ", user);
  // show thông tin
  document.getElementById("spanTenSV").innerText = user.ten;
  document.getElementById("spanMaSV").innerText = user.ma;
  document.getElementById("spanLoaiSV").innerText = user.loai;
  document.getElementById("spanDTB").innerText = user.tinhDTB();
  document.getElementById("spanXepLoai").innerText = user.xepLoai();
}
