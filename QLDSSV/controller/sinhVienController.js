function layThongTinTuForm() {
  var maSV = document.getElementById("txtMaSV").value.trim();
  var tenSV = document.getElementById("txtTenSV").value.trim();
  var email = document.getElementById("txtEmail").value.trim();
  var matKhau = document.getElementById("txtPass").value.trim();
  var diemToan = document.getElementById("txtDiemToan").value;
  var diemLy = document.getElementById("txtDiemLy").value;
  var diemHoa = document.getElementById("txtDiemHoa").value;

  var sv = new SinhVien(maSV, tenSV, email, matKhau, diemToan, diemLy, diemHoa);

  console.log("sv: ", sv.tinhDTB());
  return sv;
}
function renderDanhSachSinhVien(list) {
  var contentHTMl = "";

  list.forEach(function (item) {
    var content = `<tr>
   <td>${item.ma}</td>
   <td>${item.ten}</td>
   <td>${item.email}</td>
   <td>${item.tinhDTB()}</td>
   <td>
   <button class="btn btn-danger" onclick="xoaSV('${item.ma}')">Xoá</button>
   <button class="btn btn-warning"  onclick="suaSV('${item.ma}')" >Sửa</button>
 
   </td>
   </tr>`;
    contentHTMl += content;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTMl;
}

function timKiemViTri(id, arr) {
  // for (var index = 0; index < arr.length; index++) {
  //   var sv = arr[index];
  //   if (sv.ma == id) {
  //     return index;
  //   }
  // }
  // return -1;

  // var index = arr.findIndex(function (sv) {
  //   return sv.ma == id;
  // });
  // return index;

  return arr.findIndex(function (sv) {
    return sv.ma == id;
  });
}

function showThongTinLenForm(sv) {
  // console.log("sv: ", sv);
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

// var dog1 = {
//   name: "bull",
//   age: 2,
// };
// var dog2 = {
//   dogname: "milu",
//   age: 3,
// };

// function Dog(ten, tuoi) {
//   this.name = ten;
//   this.age = tuoi;
// }
// var dog3 = new Dog("den", 5);
// dog3.name;
// dog3.age;
// var arrDog = [dog1, dog2];
