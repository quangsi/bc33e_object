var dssv = [];
const DSSV = "DSSV";
// lấy DSSV từ localstorage dưới dạng json
let dssvLocalStorage = localStorage.getItem(DSSV);
// console.log("dssvLocalStorage: ", JSON.parse(dssvLocalStorage));
// convert json thành array và gán lại cho dssv

if (JSON.parse(dssvLocalStorage)) {
  var data = JSON.parse(dssvLocalStorage);
  console.log("before ", data);
  for (var index = 0; index < data.length; index++) {
    var current = data[index];
    var sv = new SinhVien(
      current.ma,
      current.ten,
      current.email,
      current.matKhau,
      current.diemToan,
      current.diemLy,
      current.diemHoa
    );
    dssv.push(sv);
  }

  renderDanhSachSinhVien(dssv);
}

function saveLocalStorage() {
  //  convert array thành json

  var dssvJson = JSON.stringify(dssv);
  // console.log("dssvJson: ", dssvJson);
  // lưu xuống local storage
  localStorage.setItem(DSSV, dssvJson);
}

function themSV() {
  var newSV = layThongTinTuForm();
  // newSV.ma = shortid.generate();

  // validate mã sv
  var isValid =
    validator.kiemTraRong(newSV.ma, "spanMaSV", "Mã Sv không được rỗng") &&
    validator.kiemTraMaSinhVien(newSV.ma, dssv) &&
    validator.kiemTraDoDai(newSV.ma, "spanMaSV", 6, 8) &&
    validator.kiemTraChuoiSo(newSV.ma, "spanMaSV");

  // validate tên sv
  isValid =
    isValid &
    validator.kiemTraRong(newSV.ten, "spanTenSV", "Tên Sv không được rỗng");

  // validator.kiemTraRong(newSV.ten, "spanTenSV", "Tên Sv không được rỗng") &
  if (isValid == false) {
    return;
  }
  dssv.push(newSV);
  saveLocalStorage();
  // xuất danh sách
  renderDanhSachSinhVien(dssv);
}

function xoaSV(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index: ", index);
  if (index !== -1) {
    // console.log("before", dssv.length);

    dssv.splice(index, 1);
    saveLocalStorage();
    // console.log("after", dssv.lengt);
    renderDanhSachSinhVien(dssv);
  }
}

function suaSV(id) {
  var index = timKiemViTri(id, dssv);
  if (index !== -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
}

function capNhatSV() {
  var svEdited = layThongTinTuForm();
  console.log("svEdited: ", svEdited);
  let index = timKiemViTri(svEdited.ma, dssv);

  if (index !== -1) {
    dssv[index] = svEdited;
    saveLocalStorage();

    renderDanhSachSinhVien(dssv);
  }
}
function resetForm() {
  document.getElementById("formQLSV").reset();
}
