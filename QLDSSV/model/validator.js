var validator = {
  kiemTraRong: function (valueInput, idError, message) {
    if (valueInput.trim() == "") {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraMaSinhVien: function (maSV, dssv) {
    console.log("maSV: ", maSV);
    var index = dssv.findIndex((sv) => {
      console.log();
      return sv.ma == maSV;
    });

    console.log("index: ", index);

    if (index !== -1) {
      document.getElementById("spanMaSV").innerText = "Mã SV đã tồn tại";
      return false;
    } else {
      document.getElementById("spanMaSV").innerText = "";
      return true;
    }

    // spanMaSV;
  },
  kiemTraDoDai: function (valueInput, idError, min, max) {
    // 5 đến 20

    var inputLength = valueInput.length;

    if (inputLength < min || inputLength > max) {
      document.getElementById(
        idError
      ).innerText = `Độ dài phải từ ${min} - ${max} kí tự`;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraChuoiSo: function (valueInput, idError) {
    var regex = /^[0-9]+$/;

    if (regex.test(valueInput)) {
      console.log("yes");
      document.getElementById(idError).innerText = "";

      return true;
    } else {
      document.getElementById(idError).innerText =
        "Trường này chỉ được nhập số";

      return false;
    }
  },
};
