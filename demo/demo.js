// object ~ pass by reference

var dog1 = {
  name: "bull",
  // key : value
  age: 2,
  // property
  isMarried: true,
  child: {
    name: "mun",
    age: 1,
    isMarried: false,
  },
  // method
  bark: function () {
    console.log("meo meo");
  },
};
// console.log("berfore", dog1.name);

// dog1.name = "meo";

// console.log("after", dog1.name);

dog1.bark();

var a = 2;

var b = a;

b = 5;

// a=?

var dog2 = dog1;

dog2.name = "alice";
// dynamic key

var key = "age";
dog2[key] = "alice";

console.log("dog1.name", dog1.name);

const username = "alice";

// username = "bob"; err

const user = {
  username: "alice",
};
user.username = "bob";
