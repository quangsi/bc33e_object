var dog1 = {
  name: "milu",
  age: 2,
};

var dog2 = {
  dogName: "black",
  age: 1,
};

var dogArr = [dog1, dog2];

//  lớp đối tượng

function Dog(_name, _age) {
  this.name = _name;
  this.age = _age;
  this.bark = function () {
    console.log("meo meo");
  };
}

var dog3 = new Dog("lulu", 2);
var dog4 = new Dog("lili", 1);
console.log(dog4, dog3);

dog3.bark();
